package edu.byu.hbll.tutorialproject.api;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.byu.hbll.tutorialproject.Configuration;

/**
 * This class tests the Tutorial Services class to ensure that it reacts properly to valid/invalid
 * isbns.
 * 
 * @author Zach Payne - zeus1776
 */

public class TutorialServicesTest extends JerseyTest {

  private static final Logger logger = LoggerFactory.getLogger(TutorialServicesTest.class);

  @Override
  protected Application configure() {
    Configuration config = new Configuration();
    Path[] paths = new Path[1];
    paths[0] = Paths.get("/srv/config/tutorialproject/config.yml");
    try {
      config.loadConfigFrom(paths[0]);
    } catch (IOException e) {
      logger.error(e.getMessage());
    }

    TutorialServices testObj = new TutorialServices(config);

    return new ResourceConfig().register(testObj);
  }


  /**
   * This function tests how the Tutorial Services class reacts when given a valid isbn and an
   * invalid isbn. 
   * Valid should return with the result provided.
   * Invalid should return an error 404.
   */
  @Test
  public void testIsbnInfo() {
    String valid = "0439139597";
    String invalid = "ajp439uml23k452309";
    String validResult =
        "{\"title\":\"Harry Potter and the Goblet of Fire"
            + "\",\"author\":\"J. K. Rowling\",\"publish_date\":\"2000\"}";

    // Invalid isbn
    Response badResult = target("/book/" + invalid).request().get(Response.class);
    assertEquals(badResult.getStatus(), Response.Status.NOT_FOUND);

    // Valid isbn
    String goodResult = target("/book/" + valid).request().get(String.class);
    assertEquals(goodResult, validResult);
  }
}
