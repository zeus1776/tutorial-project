package edu.byu.hbll.tutorialproject.api;

import java.io.IOException;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.byu.hbll.tutorialproject.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is utilized to pull in an isbn from a URI and, using this isbn, pull info from Open
 * Library and print to your webpage the title, author, and publish date in a json readable string.
 *
 * @author Zach Payne - zeus1776
 */
@Path("")
public class TutorialServices {

  private static Client client = ClientBuilder.newClient();
  private ObjectMapper mapper = new ObjectMapper();
  private Configuration config;
  private static final Logger logger = LoggerFactory.getLogger(TutorialServices.class);

  @Inject
  public TutorialServices(Configuration config) {
    this.config = config;
  }

  /**
   * Utilizing an isbn passed in, pulls in book info from OpenLibrary and returns the title, author,
   * and publish date.
   *
   * @param isbn Parameter given in the URI that corresponds to a specific book
   * @return Produces a json type string with the title, author, and publish date for the specified
   *     book if isbn is valid; else, returns error 404.
   */
  @GET
  @Path("/book/{isbn}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getBook(@PathParam(value = "isbn") final String isbn) {

    try {
      String test = "{}";
      String response =
          client
              .target(config.getProperty("url1") + isbn + config.getProperty("url2"))
              .request()
              .get(String.class);

      if (response.equals(test)) {
        return Response.status(Response.Status.NOT_FOUND).build();
      }

      JsonNode jsonResponse = mapper.readTree(response);
      ObjectNode bookInfo = mapper.createObjectNode();
      bookInfo.set("title", jsonResponse.path("ISBN:" + isbn).path("title"));
      bookInfo.set("author", jsonResponse.path("ISBN:" + isbn).path("authors").findPath("name"));
      bookInfo.set("publish_date", jsonResponse.path("ISBN:" + isbn).path("publish_date"));
      return Response.ok(bookInfo.toString()).build();

    } catch (IOException e) {
      logger.error(e.getMessage());
      return Response.status(Response.Status.NOT_FOUND).build();
    }
  }
}
