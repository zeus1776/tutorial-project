package edu.byu.hbll.tutorialproject.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Application class.
 */
@ApplicationPath("")
public class MyApplication extends Application {

}
